import pygame
import cv2
import os
import threading
import time


class VideoStream(threading.Thread):
    def __init__(self, w, h, images, index, screen, cap):
        threading.Thread.__init__(self)
        self._screen = screen
        self._cap = cap
        self._images = images
        self._idx = index
        self._w = w
        self._h = h

    def run(self):
        ret, img = self._cap.read()
        if ret:
            img = cv2.transpose(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
            resize = cv2.resize(img, (self._h, self._w))
            self._images[self._idx] = resize


class Display():
    def __init__(self, screenHeight, screenWidth, row, col, streams):
        self.h = screenHeight
        self.w = screenWidth
        self.rows = row
        self.cols = col
        self.vh = int(screenHeight / row)
        self.vw = int(screenWidth / col)
        self._streams = streams

    def start(self):
        pygame.init()
        self._screen = pygame.display.set_mode((self.w, self.h))
        self._screen.fill((0, 0, 0))
        pygame.display.update()

        captures = []
        for stream in self._streams:
            captures.append(cv2.VideoCapture(stream))

        while True:
            print(threading.active_count())
            threads = []
            images = [None] * len(self._streams)
            for idx, stream in enumerate(self._streams):
                t = VideoStream(self.vw, self.vh, images, idx,
                                self._screen, captures[idx])
                threads.append(t)
            for x in threads:
                x.start()
            for x in threads:
                x.join()

            for idx, stream in enumerate(self._streams):
                X = int(idx % self.cols) * self.vw
                Y = int(idx / self.cols) * self.vh
                try:
                    surface = pygame.pixelcopy.make_surface(images[idx])
                    self._screen.blit(surface, (X, Y))
                except:
                    pass

            pygame.display.flip()
            time.sleep(0.4)

    def stop(self):
        pygame.quit()


streams = [
    "rtsp://admin:Reez#1234@192.168.29.110:554/cam/realmonitor?channel=1&subtype=0",
    "rtsp://admin:Reez#1234@192.168.29.110:554/cam/realmonitor?channel=2&subtype=0",
    "rtsp://admin:Reez#1234@192.168.29.110:554/cam/realmonitor?channel=3&subtype=0",
    "rtsp://admin:Reez#1234@192.168.29.110:554/cam/realmonitor?channel=4&subtype=0",
    "rtsp://admin:Reez#1234@192.168.29.110:554/cam/realmonitor?channel=5&subtype=0",
    "rtsp://admin:Reez#1234@192.168.29.110:554/cam/realmonitor?channel=6&subtype=0",
    "rtsp://admin:Reez#1234@192.168.29.110:554/cam/realmonitor?channel=7&subtype=0",
    "rtsp://admin:Reez#1234@192.168.29.110:554/cam/realmonitor?channel=8&subtype=0"
]

WIDTH = 1280
HEIGHT = 720

ROW = 3
COL = 3

display = Display(HEIGHT, WIDTH, ROW, COL, streams)
display.start()
